import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { QuickstartedSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [QuickstartedSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [QuickstartedSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QuickstartedSharedModule {
  static forRoot() {
    return {
      ngModule: QuickstartedSharedModule
    };
  }
}
