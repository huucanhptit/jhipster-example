/**
 * View Models used by Spring MVC REST controllers.
 */
package com.evotek.quickstarted.web.rest.vm;
